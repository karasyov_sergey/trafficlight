package ru.kso.trafficlights;

import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public class MainActivity extends AppCompatActivity {
    private int idOfColor;
    private ConstraintLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("idOfColor", idOfColor);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("idOfColor")) {
            idOfColor = savedInstanceState.getInt("idOfColor+");
            resetUI();
        }
    }

    private void resetUI() {
        layout = findViewById(R.id.layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            layout.setBackgroundColor(getColor(idOfColor));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void toRedBackground(View view) {
        layout = findViewById(R.id.layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            idOfColor = R.color.colorRed;
            layout.setBackgroundColor(getColor(idOfColor));
        }

    }

    public void toYellowBackground(View view) {
        layout = findViewById(R.id.layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            idOfColor = R.color.colorYellow;
            layout.setBackgroundColor(getColor(idOfColor));
        }
    }

    public void toGreenBackground(View view) {
        layout = findViewById(R.id.layout);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            idOfColor = R.color.colorGreen;
            layout.setBackgroundColor(getColor(idOfColor));
        }
    }
}
